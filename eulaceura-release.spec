%define debug_package %{nil}
%define product_family Eulaceura
%define product_family_lo eulaceura
%define variant_titlecase Server
%define variant_lowercase server
%define dist_release_version 22H1
%define generic_version 2.0
%define generic_patch_level %{nil}
%define generic_release 1
%define builtin_release_version 1.0

%define current_arch %{_arch}
%ifarch i386
%define current_arch x86
%endif

Name:		eulaceura-release
Version:	%{generic_version}%{generic_patch_level}
Release:	%{generic_release}
Summary:	%{product_family} release file
License:	Mulan PSL v2
Provides:	eulaceura-release
Provides:	openEuler-release
Provides:	generic-release
Provides:	system-release = %{generic_version}%{generic_patch_level}
Source0:	eulaceura-release-%{builtin_release_version}.tar.gz
Source1:	85-display-manager.preset
Source2:	90-default.preset
Source3:	99-default-disable.preset

Requires:       eulaceura-repos

%description
%{product_family} release files



%prep
%setup -q -n eulaceura-release-%{builtin_release_version}

%build
echo OK

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/etc
echo "%{product_family} release %{generic_version} (%{dist_release_version})" > $RPM_BUILD_ROOT/etc/%{product_family_lo}-release
ln -sf %{product_family_lo}-release $RPM_BUILD_ROOT/etc/system-release

# create /etc/os-release
cat << EOF >>$RPM_BUILD_ROOT/etc/os-release
NAME="%{product_family}"
VERSION="%{generic_version} (%{dist_release_version})"
ID="%{product_family}"
VERSION_ID="%{generic_version}"
PRETTY_NAME="%{product_family} %{generic_version} (%{dist_release_version})"
ANSI_COLOR="0;31"

EOF

mkdir -p -m 755 $RPM_BUILD_ROOT/etc/profile.d
cp -a color.sh $RPM_BUILD_ROOT/etc/profile.d/
#cp -a system-info.sh $RPM_BUILD_ROOT/etc/profile.d/

# write cpe to /etc/system/release-cpe
echo "cpe:/o:%{product_family}:%{product_family}:%{version}:ga:server" > $RPM_BUILD_ROOT/etc/system-release-cpe

# create /etc/issue and /etc/issue.net
echo 'Welcome to \S (Linux \r)' > $RPM_BUILD_ROOT/etc/issue
echo '' >> $RPM_BUILD_ROOT/etc/issue
cp $RPM_BUILD_ROOT/etc/issue $RPM_BUILD_ROOT/etc/issue.net
echo >> $RPM_BUILD_ROOT/etc/issue

mkdir -p $RPM_BUILD_ROOT/usr/share/eula
cp eula.[!py]* $RPM_BUILD_ROOT/usr/share/eula

mkdir -p $RPM_BUILD_ROOT/var/lib
cp supportinfo $RPM_BUILD_ROOT/var/lib/supportinfo

# set up the dist tag macros
install -d -m 755 $RPM_BUILD_ROOT/etc/rpm
cat >> $RPM_BUILD_ROOT/etc/rpm/macros.dist << EOF
# dist macros.

%%dist %%{nil}
%%%{product_family} 2 
EOF

# use unbranded datadir
mkdir -p -m 755 $RPM_BUILD_ROOT/%{_datadir}/%{product_family_lo}-release
install -m 644 EULA $RPM_BUILD_ROOT/%{_datadir}/%{product_family_lo}-release

# use unbranded docdir
mkdir -p -m 755 $RPM_BUILD_ROOT/%{_docdir}/%{product_family_lo}-release
cp -p License/LICENSE $RPM_BUILD_ROOT/%{_docdir}/%{product_family_lo}-release

# copy systemd presets
mkdir -p %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE1} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE2} %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -m 0644 %{SOURCE3} %{buildroot}%{_prefix}/lib/systemd/system-preset/

%clean
rm -rf $RPM_BUILD_ROOT



%files
%defattr(0644,root,root,0755)
/etc/system-release
/etc/profile.d/color.sh
#/etc/profile.d/system-info.sh
/etc/%{product_family_lo}-release
%config(noreplace) /etc/os-release
%config /etc/system-release-cpe 
%config(noreplace) /etc/issue
%config(noreplace) /etc/issue.net
/etc/rpm/macros.dist
%{_docdir}/%{product_family_lo}-release/*
%{_datadir}/%{product_family_lo}-release/*
%{_prefix}/lib/systemd/system-preset/*
/var/lib/supportinfo
/usr/share/eula/eula.*

%changelog
* Mon Jul 04 2022 Eustace <eusteuc@outlook.com> - 2.0-1
- Version update

* Sat Jul 02 2022 Eustace <eusteuc@outlook.com> - 1.0-3
- Rearrange issue text

* Fri Jul 01 2022 Eustace <eusteuc@outlook.com> - 1.0-2
- Remove unnecessary slow scripts with its requirement

* Thu Jun 30 2022 Eustace <eusteuc@outlook.com> - 1.0-1
- Package init
