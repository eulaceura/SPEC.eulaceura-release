
Name:           eulaceura-repos
Version:        2.0
Release:        1
Summary:        Eulaceura package repositories
License:        Mulan PSL v2

Provides:       system-repos
Provides:       eulaceura-repos
Provides:       openEuler-repos
Requires:       eulaceura-gpg-keys = %{version}-%{release}

Source2:        Eulaceura.repo
Source4:        RPM-GPG-KEY-Eulaceura
Source5:        LICENSE

%description
Eulaceura package repository files along with gpg public keys

%package -n     eulaceura-gpg-keys
Summary:        Eulaceura RPM keys

%description -n eulaceura-gpg-keys
This package provides the RPM signature keys.


%prep

%build

%install
# Install the keys
install -d -m 755 $RPM_BUILD_ROOT/etc/pki/rpm-gpg
install -m 644 %{_sourcedir}/RPM-GPG-KEY* $RPM_BUILD_ROOT/etc/pki/rpm-gpg/
#mv $RPM_BUILD_ROOT/etc/pki/rpm-gpg/RPM-GPG-KEY-generic $RPM_BUILD_ROOT/etc/pki/rpm-gpg/RPM-GPG-KEY-%{vendor}

install -d -m 755 $RPM_BUILD_ROOT/etc/zypp/repos.d
install -m 644 %{_sourcedir}/Eulaceura.repo $RPM_BUILD_ROOT/etc/zypp/repos.d/Eulaceura.repo

%files
%dir /etc/zypp/repos.d
%config(noreplace) /etc/zypp/repos.d/Eulaceura.repo

%files -n eulaceura-gpg-keys
/etc/pki/rpm-gpg/

%changelog
* Mon Jul 04 2022 Eustace <eusteuc@outlook.com> - 2.0-1
- Version bump

* Fri Jul 1 2022 Eustace <eusteuc@outlook.com> - 1.0-1
- Package init
